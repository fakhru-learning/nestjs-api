import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';

@Injectable()
export class PrismaService extends PrismaClient {
    constructor(){
        super({
            datasources: {
                db: {
                    url: 'postgresql://fakhru94:fakhru94@localhost:5432/nestjs-api?schema=public'
                }
            }
        })
    }
}
